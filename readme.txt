A library of configurable layouts for Drupal 8.3+

Included Layouts:
- A configurable basic one column layout with a floating region.
- A configurable two column layout with a header, a footer, 
  and two floating regions.

Requirements
- Layout Discovery module: Part of Drupal 8.3+ core.

Roadmap
- The goal is to create an extensive library of layouts and 
  make them available to all themes.

Upcoming layouts:
- Configurable three column layouts with three child floating regions

Upcoming changes:
- Improve the configuration UI
- Make layouts responsive (depending on the core breakpoint module)

Ideas and contributions are welcome.
